import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {NewTitleComponent} from './new-title/new-title.component';
import {NotFoundComponent} from './not-found/not-found.component';
import {HomeComponent} from './home/home.component';
import {MainComponent} from './main/main.component';
import {TitleComponent} from './title/title.component';
import {UpdateTitleComponent} from './update-title/update-title.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {UserlistComponent} from './userlist/userlist.component';

const routes: Routes = [
  { path: '', redirectTo: '/main', pathMatch: 'full' },
  { path: 'home', component: HomeComponent },
  { path: 'main', component: MainComponent },
  { path: 'auth/login', component: LoginComponent },
  { path: 'auth/signup', component: RegisterComponent },
  { path: 'users', component: UserlistComponent },
  { path: 'title/:id', component: TitleComponent },
  { path: 'new_title', component: NewTitleComponent },
  { path: 'update_title/:id', component: UpdateTitleComponent },
  { path: '**', component: NotFoundComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {enableTracing: true})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
