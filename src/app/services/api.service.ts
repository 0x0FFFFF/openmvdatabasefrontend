import { Injectable } from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {Observable, of} from 'rxjs';
import {Title} from '../model/title';
import {Users} from '../model/users';
import {MainPage} from '../model/main-page';
import {TitleCountry} from '../model/title-country';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

        /** уры реста
     */
  private BASE_URL = 'http://localhost:8080/titles';
  private BASE_ADMIN_URL = 'http://localhost:8080/api';
  private BASE_USERS_URL = 'http://localhost:8080/users';
  private BASE_PAGES_URL = 'http://localhost:8080/page';

  constructor(private http: HttpClient) { }

        /** получения данных главной страницы
     */
  getMainpageData(): Observable<MainPage[]> {
    const url = `${this.BASE_PAGES_URL}/main`;
    return this.http.get<MainPage[]>(url);
  }
        /** получения данных популряности
     */
  getMainpageTitlesByNoOfAccesses(): Observable<Title[]> {
      const url = `${this.BASE_PAGES_URL}/popular`;
      return this.http.get<Title[]>(url);
  }
        /** добавление нового title
     */
  addTitle(title: Title): Observable<any> {
    const url = `${this.BASE_URL}/create`;
    return this.http.post(url, title, httpOptions);
  }
        /** получения данных title
     */
  getTitles(): Observable<Title[]> {
    const url = `${this.BASE_URL}/all`;
    return this.http.get<Title[]>(url);
  }
        /** получения данных title по популряности
     */
  getTitlesByNoOfAccesses(): Observable<Title[]> {
    const url = `${this.BASE_URL}/all/byAccesses`;
    return this.http.get<Title[]>(url);
  }
        /** получения данных всех пользователей
     */
  getUsers(): Observable<Users[]> {
    const url = `${this.BASE_USERS_URL}/all`;
    return this.http.get<Users[]>(url);
  }
        /** получения данных title определенной страны
     */
  getTitlesByCategory(titleCategory: TitleCountry): Observable<Title[]> {
    const url = `${this.BASE_URL}/all/byCategory/${titleCategory}`;
    return this.http.get<Title[]>(url);
  }
        /** получения данных конкретного title
     */
  getTitle(id: number): Observable<Title> {
    const url = `${this.BASE_URL}/title/${id}`;
    return this.http.get<Title>(url);
  }
        /** обновление данных title
     */
  updateTitle(title: Title, id: number): Observable<any> {
    const url = `${this.BASE_URL}/update/${id}`;
    return this.http.post(url, title, httpOptions);
  }
        /** удаление title
     */
  deleteTitle(id: number): Observable<any> {
    const url = `${this.BASE_URL}/delete/${id}`;
    return this.http.delete(url);
  }
        /** получения данных лайв-ввода
     */
  searchTitles(term: string): Observable<Title[]> {
    const url = `${this.BASE_URL}/all/byName/${term}`;
    if (!term.trim()) {
      return of([]);
    }
    return this.http.get<Title[]>(url);
  }
}
