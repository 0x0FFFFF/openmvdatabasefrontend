import {Component, OnInit} from '@angular/core';
import {ApiService} from '../services/api.service';
import {ActivatedRoute} from '@angular/router';
import {Title} from '../model/title';
import {Location} from '@angular/common';

@Component({
  selector: 'app-title',
  templateUrl: './title.component.html',
  styleUrls: ['./title.component.css']
})
export class TitleComponent implements OnInit {

  title: Title;
  instructions: string[] = [];
  suggestions: string[] = [];

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.route.params.subscribe(params => {
      const id = +params['id'];
      this.getTitle();
    });
  }

  getTitle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.getTitle(id).subscribe(title => {
      this.title = title;
      this.instructions = title.instructions.split('\n');
      this.suggestions = title.suggestions.split('\n');
    });
  }

  deleteThisTitle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    if (confirm('Вы правда хотите удалить запись?')) {
      this.apiService.deleteTitle(id).subscribe(
        res => {
          alert('Удаление завершено');
          this.location.back();
        },
        err => alert('Ошибка удаления записи')
      );
    }
  }
}
