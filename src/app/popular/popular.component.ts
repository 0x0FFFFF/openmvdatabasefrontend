import { Component, OnInit } from '@angular/core';
import {ApiService} from '../services/api.service';
import {Title} from '../model/title';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-popular',
  templateUrl: './popular.component.html',
  styleUrls: ['./popular.component.css']
})
export class PopularComponent implements OnInit {

  titles: Title[] = [];
  private isLoggedIn: boolean = false;

  constructor(private apiService: ApiService, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
            /** проверка авторизации
     */
    if (this.tokenStorage.getToken()) {
      this.isLoggedIn = true;
    }

    this.apiService.getMainpageTitlesByNoOfAccesses().subscribe(
      res => {
        this.titles = res;
      },
      err => {
        alert('Ошибка получения данных с сервера');
      }
    );
  }

}
