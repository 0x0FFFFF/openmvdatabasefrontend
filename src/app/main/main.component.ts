import {Component, OnInit} from '@angular/core';
import {TokenStorageService} from '../auth/token-storage.service';
import {ApiService} from '../services/api.service';
import {MainPage} from '../model/main-page';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.css']
})
export class MainComponent implements OnInit {

  info: any;
  trailer; trailerWrapper; seekBar; wrapper; replayIcon; slider; bg; poster: HTMLElement;
  curPosSlider: number;

  dataPage: MainPage[] = [];

  constructor(
    private token: TokenStorageService,
    private apiService: ApiService,
  ) { }

  ngOnInit() {
    this.info = {
      token: this.token.getToken(),
      username: this.token.getUsername(),
      authorities: this.token.getAuthorities()
    };

    this.apiService.getMainpageData().subscribe(
      res => {
        this.dataPage = res;
        console.log(this.dataPage[0]);
        this.setImages();
      },
      err => {
        alert('Ошибка получения данных с сервера');
      }
    );

    this.setVars();

    const watchTrailer = document.getElementById('watchTrailer');
    watchTrailer.addEventListener('click', (e: Event) => {
        e.preventDefault();
        this.watchTrailer();
    });

    const closeTrailer = document.getElementById('closeTrailer');
    closeTrailer.addEventListener('click', (e: Event) => {
      e.preventDefault();
      this.closeTrailer();
    });

    const slideLeft = document.getElementById('slide-2-left');
    slideLeft.addEventListener('click', (e: Event) => {
        e.preventDefault();
        this.scrollToLeft();
    });

    const slideRight = document.getElementById('slide-2-right');
    slideRight.addEventListener('click', (e: Event) => {
        e.preventDefault();
        this.scrollToRight();
    });

  }

    /** установака нового бэкграунда
     */

  setImages() {
    const bg = document.getElementById('bg');
    setTimeout(() => {
      bg.style.backgroundImage = 'url(' + this.dataPage[0].background + ')';
    });
    
    
    //bg.setAttribute('style', 'background-image: ' + this.dataPage[0].background);

    const poster = document.getElementById('contentPoster');
    poster.setAttribute('src', this.dataPage[0].poster);
    console.log(this.dataPage[0].poster);
  }

    /** привязка к элементам
     */

  setVars() {
    this.trailer = document.getElementById('trailer__player');
    this.trailerWrapper = document.getElementById('trailer');
    this.seekBar = document.getElementById('seekbar');
    this.wrapper = document.getElementById('player');
    this.replayIcon = document.getElementById('replay');

    this.slider = document.getElementById('slider');
    this.curPosSlider = 0;
  }

      /** запуск плеера
     */

  watchTrailer() {
    this.onPlay();
    this.trailerWrapper.style.display = 'block';
    this.trailer.setAttribute('src', this.dataPage[0].trailer_url);
    this.trailer.play();
    this.trailer.controls = false;
    }

      /** закрытие плеера
     */

  closeTrailer() {
    this.trailer.pause();
    this.trailer.currentTime = 0;
    this.trailerWrapper.style.display = 'none';
  }

        /** запуск видео
     */

  onPlay() {
    this.replayIcon.style.display = 'none';
  }

      /** пауза плеера
     */
  onPause(callback: () => void): void {
    if (this.trailer.paused) {
      this.trailer.play();
      this.replayIcon.style.display = 'none';
    } else {
      this.trailer.pause();
    }
  }

      /** обработка события на окончание воспроизведения плеера
     */

  onEnded(callback: () => void): void {
//  this.trailer.style.pointerEvents = 'none';
    this.trailer.controls = false;
    this.replayIcon.style.display = 'block';
  }


      /** событие на таймлайне
     */

  updateTime(callback: () => void): void {
    this.seekBar.style.width = (this.trailer.currentTime + .25) / this.trailer.duration * 100 + '%';
  }

      /** карусель влево
     */

  scrollToLeft() {
    if (this.curPosSlider > 0) {
      for (let i = 0; i < 80; i++) {
          this.curPosSlider = this.curPosSlider - .25;
          this.slider.style.marginLeft = '-'  + this.curPosSlider + '%';
      }
    }
  }

      /** карусель вправо
     */
  scrollToRight() {
    if (this.curPosSlider < 100) {
      for (let i = 0; i < 80; i++) {
          this.curPosSlider = this.curPosSlider + .25;
          this.slider.style.marginLeft = '-' + this.curPosSlider + '%';
      }
    }
  }

}
