import { Component, OnInit } from '@angular/core';
import {ApiService} from '../services/api.service';
import {Title} from '../model/title';
import {TitleCountry} from '../model/title-country';
import {OptionCategory} from '../model/option-category';

@Component({
  selector: 'app-titles',
  templateUrl: './titles.component.html',
  styleUrls: ['./titles.component.css']
})
export class TitlesComponent implements OnInit {

    titles: Title[] = [];

  opts: OptionCategory[] = [
    { id: TitleCountry.KR, name: 'Корея' },
    { id: TitleCountry.JP, name: 'Япония' },
    { id: TitleCountry.CN, name: 'Китай' }
  ];

  constructor(private apiService: ApiService) { }

  ngOnInit() {
    this.getAllTitles();
  }

  getAllTitles() {
    this.apiService.getTitles().subscribe(
      res => {
        this.titles = res;
      },
      err => {
        alert('Ошибка получения данных с сервера');
      }
    );
  }

  getAllByCategory(titleCategory: TitleCountry) {
    this.apiService.getTitlesByCategory(titleCategory).subscribe(
      res => {
        this.titles = res;
      },
      err => {
        alert('Ошибка получения данных с сервера');
      }
    );
  }
}
