import {Component, OnInit} from '@angular/core';
import {Title} from '../model/title';
import {TitleCountry} from '../model/title-country';
import {ApiService} from '../services/api.service';
import {Location} from '@angular/common';
import {Award} from '../model/award';
import {OptionCategory} from '../model/option-category';

@Component({
  selector: 'app-new-title',
  templateUrl: './new-title.component.html',
  styleUrls: ['./new-title.component.css']
})
export class NewTitleComponent implements OnInit {

  model: Title = {
    titleId: 0,
    titleName: '',
    image: '',
    contentCountry: TitleCountry.KR,
    awardsList: [],
    instructions: '',
    suggestions: '',
    lastAccessed: '',
    noOfTimesAccessed: 0
  };

  award: Award = {
    awardId: 0,
    awardName: '',
    quantity: ''
  };

  opts: OptionCategory[] = [
    { id: TitleCountry.KR, name: 'Корея' },
    { id: TitleCountry.JP, name: 'Япония' },
    { id: TitleCountry.CN, name: 'Китай' }
  ];

  filterCategory(theCategory: TitleCountry) {
    this.model.contentCountry = theCategory;
  }

  constructor(
    private apiService: ApiService,
    private location: Location) { }

  ngOnInit() {
  }

        /** отправка данных на сервер
     */

  sendTitle(): void {
    this.apiService.addTitle(this.model).subscribe(
      res => {
        this.location.back();
      },
      err => {
        alert('Произошла ошибка при отправке данных');
      }
    );
  }


        /** добавление номинации или награды
     */
  addToAwardsList(): void {
    this.model.awardsList.push(JSON.parse(JSON.stringify(this.award)));
  }

}
