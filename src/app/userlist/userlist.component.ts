import { Component, OnInit } from '@angular/core';
import {ApiService} from '../services/api.service';
import {Users} from '../model/users';
import {TokenStorageService} from '../auth/token-storage.service';

@Component({
  selector: 'app-userlist',
  templateUrl: './userlist.component.html',
  styleUrls: ['./userlist.component.css']
})
export class UserlistComponent implements OnInit {
  users: Users[] = [];
  roles: string[] = [];

  constructor(private apiService: ApiService, private tokenStorage: TokenStorageService) { }

  ngOnInit() {
    this.roles = this.tokenStorage.getAuthorities();
    console.log(this.roles);
    if (this.roles[0] === 'ADMIN') {
      this.getUsers();
    } else {
      alert('Нет доступа');
    }
  }

  getUsers() {
    this.apiService.getUsers().subscribe(
      usr => {
        this.users = usr;
      },
      err => {
        alert('Ошибка получения данных пользователей');
      }
    );
  }

}
