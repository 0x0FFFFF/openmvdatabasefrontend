import {Component, Input, OnInit} from '@angular/core';
import {Title} from '../model/title';
import {ApiService} from '../services/api.service';
import {ActivatedRoute} from '@angular/router';
import {Award} from '../model/award';
import {TitleCountry} from '../model/title-country';
import {OptionCategory} from '../model/option-category';
import {Location} from '@angular/common';

@Component({
  selector: 'app-update-title',
  templateUrl: './update-title.component.html',
  styleUrls: ['./update-title.component.css']
})
export class UpdateTitleComponent implements OnInit {

  @Input() title: Title;

  award: Award = {
    awardId: 0,
    awardName: '',
    quantity: ''
  };

  opts: OptionCategory[] = [
    { id: TitleCountry.KR, name: 'Корея' },
    { id: TitleCountry.JP, name: 'Япония' },
    { id: TitleCountry.CN, name: 'Китай' }
  ];

  filterCategory(theCategory: TitleCountry) {
    this.title.contentCountry = theCategory;
  }

  constructor(
    private apiService: ApiService,
    private route: ActivatedRoute,
    private location: Location
  ) { }

  ngOnInit() {
    this.getTitle();
  }

  getTitle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.getTitle(id)
      .subscribe(title => this.title = title);
  }

  addToAwardsList(): void {
    this.title.awardsList.push(JSON.parse(JSON.stringify(this.award)));
  }

  deleteFromAwardsList(award: Award) {
    const indexOfIngredient = this.title.awardsList.indexOf(award);
    this.title.awardsList.splice(indexOfIngredient, 1);
  }

  sendTitle(): void {
    const id = +this.route.snapshot.paramMap.get('id');
    this.apiService.updateTitle(this.title, id).subscribe(
      res => {
        this.location.back();
      },
      err => {
        alert('Ошибка обновления материала');
      }
    );
  }
}
