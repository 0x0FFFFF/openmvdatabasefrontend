export interface Award {
  awardId: number;
  awardName: string;
  quantity: string;
}
