import {TitleCountry} from './title-country';
import {Award} from './award';

export interface Title {
  titleId: number;
  titleName: string;
  image: string;
  contentCountry: TitleCountry;
  awardsList: Award[];
  instructions: string;
  suggestions: string;
  lastAccessed: string;
  noOfTimesAccessed: number;
}
