import {Role} from './role';

export interface Users {
  id: number;
  name: string;
  username: string;
  email: string;
  role: Role[];
}
