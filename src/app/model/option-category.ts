import {TitleCountry} from './title-country';

export interface OptionCategory {
  id: TitleCountry;
  name: string;
}
