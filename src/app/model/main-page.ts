export interface MainPage {
  background: string;
  top_title: string;
  bottom_title: string;
  trailer_url: string;
  link_to_watch: string;
  text_to_watch: string;
  poster: string;
  date_text: string;
  time: string;
  text: string;
}
