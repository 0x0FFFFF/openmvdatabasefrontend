import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavigationComponent } from './navigation/navigation.component';
import { NewTitleComponent } from './new-title/new-title.component';
import { TitlesComponent } from './titles/titles.component';
import { NotFoundComponent } from './not-found/not-found.component';
import { TitleComponent } from './title/title.component';

import { HomeComponent } from './home/home.component';
import { UpdateTitleComponent } from './update-title/update-title.component';
import { SearchTitleComponent } from './search-title/search-title.component';
import { LoginComponent } from './login/login.component';
import { RegisterComponent } from './register/register.component';

import { httpInterceptorProviders } from './auth/auth-interceptor';
import { PopularComponent } from './popular/popular.component';
import { MainComponent } from './main/main.component';
import { FooterComponent } from './footer/footer.component';
import { UserlistComponent } from './userlist/userlist.component';
import { FooterLightComponent } from './footer-light/footer-light.component';
import { UpdateRoleComponent } from './update-role/update-role.component';

@NgModule({
  declarations: [
    AppComponent,
    NavigationComponent,
    NewTitleComponent,
    TitlesComponent,
    NotFoundComponent,
    TitleComponent,
    HomeComponent,
    UpdateTitleComponent,
    SearchTitleComponent,
    LoginComponent,
    RegisterComponent,
    PopularComponent,
    MainComponent,
    FooterComponent,
    UserlistComponent,
    FooterLightComponent,
    UpdateRoleComponent,
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    FormsModule,
    AppRoutingModule
  ],
  providers: [httpInterceptorProviders],
  bootstrap: [AppComponent]
})
export class AppModule { }
